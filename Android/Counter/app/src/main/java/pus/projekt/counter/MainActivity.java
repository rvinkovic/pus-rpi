package pus.projekt.counter;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pubnub.api.Pubnub;

public class MainActivity extends ActionBarActivity {
    String channel = "demo";
    String pubKey = "pub-c-4904d6fd-fd46-443c-b4e1-e983daf744ea";
    String subKey = "sub-c-6c237a84-f707-11e4-b3e1-0619f8945a4f";
    int tmpRoom = 0;
    PubnubHandler pubnubHandler = new PubnubHandler(channel, pubKey,subKey, this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        pubnubHandler.startSubscribe();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            changeSettings(this);
            return true;
        }else if (id == R.id.exit) {
            pubnubHandler.stopSubscribe();
            finish();
            System.exit(0);
            return true;
        }else if (id == R.id.reset) {
            startFrom(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void changeSettings(final MainActivity ma) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle("Settings");
        alert.setMessage("Channel name:");

        final EditText channelname = new EditText(this);
        final EditText pubKeyInput = new EditText(this);
        final EditText subKeyInput = new EditText(this);

        channelname.setHint("Channel name:");
        pubKeyInput.setHint("Publish key:");
        subKeyInput.setHint("Subscribe key:");

        channelname.setText(channel);
        pubKeyInput.setText(pubKey);
        subKeyInput.setText(subKey);

        LinearLayout ll=new LinearLayout(this);
        ll.setOrientation(LinearLayout.VERTICAL);
        ll.addView(channelname);
        ll.addView(pubKeyInput);
        ll.addView(subKeyInput);
        alert.setView(ll);

        alert.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                channel = String.valueOf(channelname.getText());
                pubKey = String.valueOf(pubKeyInput.getText());
                subKey = String.valueOf(subKeyInput.getText());
                pubnubHandler.stopSubscribe();
                pubnubHandler = new PubnubHandler(channel, pubKey,subKey, ma);
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // Canceled.
            }
        });
        alert.show();
    }

    public void changeTextView(final String[] parts){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView txtUkupno = (TextView) findViewById(R.id.nmbViewerUkupno);
                txtUkupno.setText(parts[2]);

                TextView txtUlaz = (TextView) findViewById(R.id.nmbViewerUlaz);
                txtUlaz.setText(parts[0]);

                TextView txtIzlaz = (TextView) findViewById(R.id.nmbViewerIzlaz);
                txtIzlaz.setText(parts[1]);
            }
        });
    }

    void startFrom(final MainActivity ma){
        final AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle("Start from number");
        alert.setMessage("People in room:");

        final EditText numberFrom = new EditText(this);

        numberFrom.setHint("Number");

        LinearLayout ll=new LinearLayout(this);
        ll.setOrientation(LinearLayout.VERTICAL);
        ll.addView(numberFrom);
        alert.setView(ll);

        alert.setPositiveButton("Set", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                try {
                    tmpRoom = Integer.parseInt(String.valueOf(numberFrom.getText()));
                    pubnubHandler.sendCommand("reset#"+tmpRoom);
                } catch (NumberFormatException e) {
                    alertNotInt(ma);
                }
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // Canceled.
            }
        });
        alert.show();
    }

    public void alertNotInt(final MainActivity ma){
        AlertDialog.Builder alertNotInt = new AlertDialog.Builder(this);
        alertNotInt.setTitle("Warning!");
        alertNotInt.setMessage("Not an Integer!");
        alertNotInt.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                startFrom(ma);
            }
        });
        alertNotInt.show();
    }

}
