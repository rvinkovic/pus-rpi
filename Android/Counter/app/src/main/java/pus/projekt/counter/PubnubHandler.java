package pus.projekt.counter;

import android.util.Log;

import com.pubnub.api.*;

public class PubnubHandler {
    MainActivity ma;
    String channel;
    String pubKey;
    String subKey;
    Pubnub pubnub;

    public PubnubHandler(String channel, String pubKey, String subKey, MainActivity ma) {
        this.channel = channel;
        this.pubKey = pubKey;
        this.subKey = subKey;
        pubnub = new Pubnub(pubKey,subKey);
        this.ma = ma;
    }



    public void startSubscribe () {

        try {
            pubnub.subscribe(channel, new Callback() {

                        @Override
                        public void connectCallback(String channel, Object message) {
                            Log.d("PUBNUB", "SUBSCRIBE : CONNECT on channel: " + channel
                                    + " : " + message.getClass() + " : "
                                    + message.toString());
                        }

                        @Override
                        public void disconnectCallback(String channel, Object message) {
                            Log.d("PUBNUB", "SUBSCRIBE : DISCONNECT on channel: " + channel
                                    + " : " + message.getClass() + " : "
                                    + message.toString());
                        }

                        public void reconnectCallback(String channel, Object message) {
                            Log.d("PUBNUB", "SUBSCRIBE : RECONNECT on channel: " + channel
                                    + " : " + message.getClass() + " : "
                                    + message.toString());
                        }

                        @Override
                        public void successCallback(String channel, Object message) {
                            Log.d("PUBNUB", "SUBSCRIBE : " + channel + " : "
                                    + message.getClass() + " : " + message.toString());
                            String string = message.toString();
                            String[] parts = string.split("#");
                            ma.changeTextView(parts);
                        }

                        @Override
                        public void errorCallback(String channel, PubnubError error) {
                            Log.d("PUBNUB", "SUBSCRIBE : ERROR on channel " + channel
                                    + " : " + error.toString());
                        }
                    }
            );
        } catch (PubnubException e) {
            e.printStackTrace();
        }
    }

    void sendCommand(String msg) {
        Callback cb = new Callback() {
            @Override
            public void successCallback(String channel, Object response) {
                Log.d("CB:", response.toString());
            }

            @Override
            public void errorCallback(String s, PubnubError pubnubError) {
                Log.d("CB:", pubnubError.toString());
            }
        };
        pubnub.publish("action", msg, cb);
    }

    public void stopSubscribe(){
        pubnub.unsubscribe(channel);
        Log.d("PUBNUB", "UNSUBSCRIBED : " + channel);
    }
}
