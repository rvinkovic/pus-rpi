#!/usr/bin/python
# -*- coding: utf-8 -*-
import Tkinter as tk
from Pubnub import Pubnub

import threading 
import Queue
import time
import string
class GUI():
    def __init__(self,parent):
        self.parent=parent       
        self.initialize()
        
    def initialize(self):
        
        self.photo_quit=tk.PhotoImage(file="quit.gif")
        self.photo_reset=tk.PhotoImage(file="reset.gif") 
         
        self.boja="white"
        self.parent.configure(background=self.boja)
        self.parent.String_People_count=tk.StringVar() #stringovi u koje se pohranjuju podaci za izmjenu na GUI-u
        self.parent.String_People_in=tk.StringVar()
        self.parent.String_People_out=tk.StringVar()
         
        self.parent.String_People_count.set("0") 
        self.parent.String_People_in.set("0")
        self.parent.String_People_out.set("0")
        
        self.parent.geometry('450x450+200+200')
        self.parent.title("People counter")
        
       
        
        
        self.parent.columnconfigure(0, weight=1)
        self.parent.columnconfigure(1, weight=1)
        self.parent.rowconfigure(0, weight=1)
        self.parent.rowconfigure(1, weight=1)
        self.parent.rowconfigure(2, weight=1)
        self.parent.rowconfigure(3, weight=1)
        self.parent.rowconfigure(4, weight=1)
        self.parent.rowconfigure(5, weight=1)
        
        
        self.parent.background_image=tk.PhotoImage(file="riteh.gif")
        background_label = tk.Label(self.parent, image=self.parent.background_image, bg=self.boja)
        background_label.grid(row=0, column=0, columnspan=2)
        tk.Label(self.parent, text="Broj osoba u prostoriji", font=("Helvetica",34),bg=self.boja).grid(row=1, column=0, columnspan=2,sticky="nsew")
        
        self.parent.People_count=tk.Label(textvariable=self.parent.String_People_count,  font=("Helvetica",40), fg="red",bg=self.boja).grid(row=2, column=0, columnspan=2)
        
        tk.Label(self.parent, text="Ušli", font=("Helvetica",30),bg=self.boja).grid(row=3, column=0)
        self.parent.People_in=tk.Label(textvariable=self.parent.String_People_in, font=("Helvetica",30),bg=self.boja).grid(row=4, column=0)
        
        tk.Label(self.parent, text="Izašli", font=("Helvetica",30),bg=self.boja).grid(row=3, column=1)
        self.parent.People_out=tk.Label(textvariable=self.parent.String_People_out, font=("Helvetica",30),bg=self.boja).grid(row=4, column=1)
        
        
        tk.Button(self.parent, text="Stop",image=self.photo_reset, command=self.popup_w).grid(row=5, column=0, columnspan=2)
        
    def change_text(self,data):
            self.parent.String_People_count.set(data[2])
            self.parent.String_People_in.set(data[0])
            self.parent.String_People_out.set(data[1])

    def popup_w(self):
		#popup
        self.parent.popup_window = tk.Toplevel ()
        self.parent.popup_window.title("Broj")
        self.parent.popup_window.geometry('170x80+350+350')#velicina prozora
        
        self.parent.msg = tk.Label(self.parent.popup_window, text="Unesite broj ljudi uprostoriji:", font=("Helvetica",10),justify="center")
        self.parent.msg.grid(row=0, column=0)
               
        self.parent.entry = tk.Entry(self.parent.popup_window)
        self.parent.entry.grid(row=1, column=0)
                    

        self.parent.button = tk.Button(self.parent.popup_window, text="Reset", command=self.ispis)#command=self.parent.popup_window.destroy
        print 1
        self.parent.button.grid(row=2, column=0)

    def ispis(self):
	    
        global pubnub
        self.parent.v=self.parent.entry.get()
        print self.parent.v
        msg="reset#"+str(self.parent.v)
        pubnub.publish("action", msg)
        self.parent.popup_window.destroy()
def callback(message, channel):
    global app
    data=message.split("#")
    app.change_text(data)    
	 
def SendReset():
	global pubnub
	pubnub.publish("action", "reset")
if __name__=="__main__":
    top=tk.Tk()  
    
    app=GUI(top)
    
    pubnub = Pubnub(publish_key="pub-c-4904d6fd-fd46-443c-b4e1-e983daf744ea", subscribe_key="sub-c-6c237a84-f707-11e4-b3e1-0619f8945a4f")
    pubnub.subscribe("demo", callback=callback)
    top.mainloop()
