#!/usr/bin/env python
import socket
import time
from Pubnub import Pubnub
import os
import signal
import sys
import errno

def callback(message):
    print "Pubnub callback"+str(message)


def _callback(message, channel):
    global people_start

    os.system('echo \'{ "command":"reset_all" }\' | nc odroid.local 5555')
    people_start=message.split('#')[1]


if len(sys.argv)==1:
  try:

        host='odroid.local'
        port=5555
        people_start="0"

        s=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            s.connect((host,port))
        except socket.error, exc:
            print "Cannot connect to socket. Check connection to sensor : %s" % exc
            os._exit(0)

        channel='demo' #ime kanal na koji se spajamo
        pubnub = Pubnub(publish_key="pub-c-4904d6fd-fd46-443c-b4e1-e983daf744ea", subscribe_key="sub-c-6c237a84-f707-11e4-b3e1-0619f8945a4f")
        pubnub.subscribe("action", callback=_callback)

        while 1:

            data=s.recv(5000)
            in_location = data.find('"in":')
            out_location=data.find('"out":')
            in_string=""
            out_string=""

            if in_location!=-1:
                i=in_location+5
                while data[i]!=',':
                    in_string=in_string+data[i]
                    i=i+1

            if out_location!=-1:
                i=out_location+6
                while data[i]!=',':
                    out_string=out_string+data[i]
                    i=i+1


            if out_string!="" and in_string!="":
                total=int(in_string)-int(out_string)+int(people_start)
                send_data=str(in_string)+"#"+str(out_string)+"#"+str(total)
                pubnub.publish(channel, send_data)
                time.sleep(1)

  except KeyboardInterrupt:
    pubnub.unsubscribe(channel="action")
    pubnub.unsubscribe(channel="demo")
    print "Terminating"
    s.close()
    os._exit(1)

######################################################DEBUG MODE
elif sys.argv[1]=="--debug":
  try:

        host='odroid.local'
        port=5555
        people_start="0"

        s=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            s.connect((host,port))
        except socket.error, exc:
            print "Cannot connect to socket. Check connection to sensor : %s" % exc
            os._exit(0)

        channel='demo' #ime kanal na koji se spajamo
        pubnub = Pubnub(publish_key="pub-c-4904d6fd-fd46-443c-b4e1-e983daf744ea", subscribe_key="sub-c-6c237a84-f707-11e4-b3e1-0619f8945a4f")
        pubnub.subscribe("action", callback=_callback)

        while 1:

            data=s.recv(5000)
            print "Sensor data:\n"+str(data)
            in_location = data.find('"in":')
            out_location=data.find('"out":')
            in_string=""
            out_string=""

            if in_location!=-1:
                i=in_location+5
                while data[i]!=',':
                    in_string=in_string+data[i]
                    i=i+1

            if out_location!=-1:
                i=out_location+6
                while data[i]!=',':
                    out_string=out_string+data[i]
                    i=i+1


            if out_string!="" and in_string!="":
                print "Number of people that entered the room:"+str(in_string)
                print "People that left the room:"+str(out_string)
			    
                total=int(in_string)-int(out_string)+int(people_start)
                print "Number of people inside the room:"+ str(total)
                send_data=str(in_string)+"#"+str(out_string)+"#"+str(total)
                print "Data for send:"+str(send_data)
                pubnub.publish(channel, send_data, error=callback, callback=callback)
                time.sleep(1)

  except KeyboardInterrupt:
    pubnub.unsubscribe(channel="action")
    pubnub.unsubscribe(channel="demo")
    print "Terminating"
    s.close()
    os._exit(1)

else:
	print "Invalid arguments. Run with --debug for debug mode"

