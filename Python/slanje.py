# -*- coding: utf-8 -*-
"""
Created on Wed May 13 21:12:58 2015

@author: NoNe666
"""
from Pubnub import Pubnub
import time


pubnub = Pubnub(publish_key="pub-c-4904d6fd-fd46-443c-b4e1-e983daf744ea", subscribe_key="sub-c-6c237a84-f707-11e4-b3e1-0619f8945a4f", ssl_on=False)

channel = 'demo'
message = 'Hello World !!!'

# Synchronous usage
#print pubnub.publish(channel='demo', message='Hello World !!!')

# Asynchronous usage

def callback(message):
    print(message)


for i in range(1,100):
    pubnub.publish(channel, i, callback=callback, error=callback)
    time.sleep(1)
    
pubnub.unsubscribe(channel)
